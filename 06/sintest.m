% sinのマクローリン展開の検算
function y = sintest(x)
y = zeros(1,4);

y(1) = x;                          % 1次で打ち切り
y(2) = y(1) - x^3/factorial(3);     % 3次
y(3) = y(2) + x^5/factorial(5);     % 5次
y(4) = y(3) + x^7/factorial(7);     % 7次
end
