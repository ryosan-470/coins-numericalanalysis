% sin(x)とsin(x+2pi)の関係を利用
function y = mysin2(n, x)

if rem(n,2) == 0
    n = n - 1;
end

x = mod(x,2*pi);
y = x;
w = x;

x2 = -x.^2; 

for k=3:2:n
    w = w .* x2 /((k)*(k-1));
    y = y + w;
end
end
