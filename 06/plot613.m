% (1-3)
x = linspace(0,pi,1000);

hold on;
% n = 1
h = plot(x,mysin(1,x), 'm');
% n = 3
h = plot(x,mysin(3,x), 'c');
% n = 5
h = plot(x,mysin(5,x), 'y');
% n =7
h = plot(x,mysin(7,x), 'b');
% sin(x)
h = plot(x,sin(x),'r');
% ラベル
label = legend('n=1', 'n=3', 'n=5', 'n=7', 'sin(x)');
set(label, 'Location', 'NorthWest');
hold off;
grid on;
saveas(h,'../matlab_path/06/plot613', 'pdf');
