n = 1000;
xl = -50;
xr = 50;

x = linspace(xl,xr,1000);

% alpha = exp(x);            % 真�?
beta1  = myexp(n,x);
beta2  = myexp2(n,x);
beta3  = myexp3(n,x);

error1 = abs(exp(x) - beta1)./exp(x);
error2 = abs(exp(x) - beta2)./exp(x);
error3 = abs(exp(x) - beta3)./exp(x);

% h = semilogy(x,error1,'r',x,error2,'c',x,error3,'y');
h = semilogy(x,error1,'r');
hold on;
h = semilogy(x,error2,'c');
h = semilogy(x,error3,'y');

legend('myexp', 'myexp2', 'myexp3');
title('n=1000, x = [-50,50]');
ylim([0,10^300]);
saveas(h, '~/public_html/secure_htdocs/matlab/06/error624_n_1000_x_50', 'pdf');