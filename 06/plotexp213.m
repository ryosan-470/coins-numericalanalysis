% 課題2(2-1-3) n = 200としてmyexp関数とMATLABのexp関数を描画する
x = linspace(-50,50,1000);
hold on;
ylim([-5*10^3, 5*10^3]);
h = plot(x,exp(x),'r');
h = plot(x,myexp(200,x), 'k');
hold off;

saveas(h, '../matlab_path/06/exp_and_myexp', 'pdf')
