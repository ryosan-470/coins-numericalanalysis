% 多項式の次数n, 値xを渡しyを返す
function y = mycos(n, x)

y = 1;
w = 1;
x2 = -x.^2;

for k=2:2:2*n-2
    w = w .* x2 / (k * (k-1));
    y = y + w;
end
end