% 修正版exp2
function y = myexp2(n,x)
y = 1;
w = 1;

f = find(x < 0);
x = abs(x);
for k=1:n
    w = w .* x./(k);
    y = y + w;
end
y(f) = 1./y(f);
end
