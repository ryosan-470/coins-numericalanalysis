% 多項式の次数n, 値xを渡しyを返す
function y = mysin(n, x)
    y = x;
    w = x;
    
    if rem(n,2) == 0
        n = n - 1;
    end
    
    x2 = -x.^2; 
    
    for k=3:2:n
        w = w .* x2 /((k)*(k-1));
        y = y + w;
    end
end
