function y = myexp3(n,x)
w = 1;  y = 1;  e = exp(1);
 
integer = fix(x);      % 整数部
decimal = x - integer; % 小数部

for k=1:n
    w = w .* decimal/(k);
    y = y + w;
end

y = e.^integer .* y;
