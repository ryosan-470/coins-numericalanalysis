% myexpとexpの相対誤差をプロットする
x = linspace(-50,50,1000);
n = 200;

alpha = exp(x);          % 真の値
beta  = myexp(n,x);      % 測定値

error = abs(beta - alpha) ./ alpha;

h = semilogy(x,error);

saveas(h, '../matlab_path/06/error621', 'pdf');
