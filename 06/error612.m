x = pi/4;
n = 1:2:15;

alpha = sin(x);
hold on;
for k=1:2:15
    beta  = mysin(k,x);
    error = abs(alpha - beta)./alpha;
    h = semilogy(k,error,'o');
end
hold off;

saveas(h, '../matlab_path/06/error612', 'pdf');