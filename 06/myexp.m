% expの値を返す
function y = myexp(n,x)
y = 1;
w = 1;

for k=1:n
    w = w .* x/(k);
    y = y + w;
end
