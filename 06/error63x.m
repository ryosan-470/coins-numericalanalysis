x = linspace(0,4*pi,2000);

error = (abs(sin(x)-mysin2(200,x)))./abs(sin(x));

h = semilogy(x,error);
saveas(h, '~/public_html/secure_htdocs/matlab/06/error631', 'pdf');

