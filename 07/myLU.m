function [L,U] = myLU(A)
n = length(A);
L = eye(n,n);
U = A;
for k = 1:n-1
    for j = 1:n-1
        % L(j+1,k) = A(j+1,k)/A(j,k);
        if k+j >= n + 1
            break;
        end
       L(k+j,k) = U(k+j,k)/U(k,k); 
       U(k+j,:) =-U(k+j,k)/U(k,k).*U(k,:) + U(k+j,:);
    end
end
end
