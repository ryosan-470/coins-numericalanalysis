function P = Pij(i, j, alpha, n)
	I = eye(n,n);
	P = I + alpha*I(:,i)*I(:,j)';
end
