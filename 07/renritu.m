function [x,error] = renritu(A,b)
[L,U] = myLU(A);

n = length(A);
y = zeros(1,n)';
for i=1:n
    c = 0;
   for j=1:i-1
       c = c + L(i,j)*y(j);
   end
   y(i) = b(i)-c;
   c = 0;
end

x = zeros(1,n)';

for i=n:-1:1
    c = 0;
    for j=i+1:n
        c = c + U(i,j)*x(j);
    end
    x(i) = (y(i) - c) * 1/U(i,i);
   
end
error = (b-A*x)' * (b-A*x);
end