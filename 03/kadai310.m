% 演習3(1-1)
f = [1 -2 0 1];
% f(1) scalar
x = 1;
y = polyval(f,x)
% f(2) scalar
x = 2;
y = polyval(f,x)
% f(3) scalar
x = 3;
y = polyval(f,x)

% f(1) f(2) f(3) vector
x = [1,2,3];
y = polyval(f,x)

z = roots(f)