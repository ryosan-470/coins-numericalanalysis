% 課題2 (2-1)
format long e
% x^3 - 124x +1 = 0
a = 1; b = -124; c = 1;f = [a b c];
% 実際に求めた値
x1 = (-b + sqrt(b^2 - 4 * a * c)) / 2 * a
x2 = (-b - sqrt(b^2 - 4 * a * c)) / 2 * a
% 真の値
z = roots(f)
% 絶対誤差
ab_err1 = abs(x1 - z(1))
ab_err2 = abs(x2 - z(2))
% 相対誤差
re_err1 = ab_err1 / abs(z(1))
re_err2 = ab_err2 / abs(z(2))
% 残差
residual1 = abs(polyval(f, x1))
residual2 = abs(polyval(f, x2))
