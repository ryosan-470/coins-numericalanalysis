# 数値計算法 演習課題3
## 課題 1 ##
MATLAB の `polyval(p,x)` 関数,及び `roots(p)`` 関数の使い方を確認す
る.これらの関数を用いて以下の問題を行いなさい.
### (1-1) ###
3次式f(x) =x^3-2x^2+1にたいしてf(1), f(2), f(3)がスカラーの場合とベクトルの場合の 2 通りで試しなさい.またレポートには両方の結果を載せなさい.

### (1-2) ###
3次方程式 f(x) =x^3-2x^2+1=0 の解を `roots(p)`を用いて求めなさい.

## 課題2 ##
2次方程式の会の公式を例として,残差,絶対誤差,相対誤差,桁落ちについて理解をする.以下の問題を行いなさい

### (2-1) ###
2次方程式 x^2 − 124x + 1 = 0 の解を解の公式で計算し,残差,絶対誤差,相対誤差を求めなさい.なお,誤差の計算では, MATLAB の roots 関数によって得られた解を正しいものと仮定する.

### (2-2) ###
(2-1) と同じ方程式の解を授業で示した桁落ちを防ぐ公式を用いて計算し,残差,絶対誤差,相対誤差を求めて (2-1) の結果と比較しなさいまた,係数にどのような関係があるときに解の公式で求めた結果が悪くなるか考察しなさい.

