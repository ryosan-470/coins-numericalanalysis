% QRå?§£ãç¨ã?æ?°äºä¹è¿ä¼¼
function [c] = useQRdolsm(A, f)
    [Q,R] = qr(A,0);
    
    c = R\(Q'*f');
end
