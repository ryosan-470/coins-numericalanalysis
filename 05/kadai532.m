% 演習5　課題3 (3-2)
temperature = [-195, 0,    20,    100,  200,  300, 497, 700, 977];
resistance  = [0.2,  1.55, 1.694, 2.33, 2.93, 3.6, 4.6, 6.7, 8.1].*10^(-8);
n = 2;         % 次数1

A = generateMatrix(n, temperature);

cx = useQRdolsm(A, resistance)

c = fliplr(cx.');

xr = linspace(-200,1050,1000);

fx = polyval(c,xr);
hold on;
h = plot(xr, fx);
h = plot(temperature, resistance, 'x');
hold off;
saveas(h, '../matlab_path/05/kadai532', 'pdf');