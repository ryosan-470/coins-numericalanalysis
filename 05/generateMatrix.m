% 演習5 課題1 (1-2)
function [A] = generateMatrix(n, x)
    m = length(x);                               % m行
    A = zeros(m, n);
    for j=1:n
        A(:,j) = (x').^(j-1);
    end
end
