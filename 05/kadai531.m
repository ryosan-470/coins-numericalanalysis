% 演習5　課題3 (3-1)
temperature = [-195, 0,    20,    100,  200,  300, 497, 700, 977];
resistance  = [0.2,  1.55, 1.694, 2.33, 2.93, 3.6, 4.6, 6.7, 8.1];

h = plot(temperature, resistance.*10^(-8), 'x');

saveas(h, '../matlab_path/05/kadai531', 'pdf');