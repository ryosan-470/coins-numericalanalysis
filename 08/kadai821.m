clear all;
i = complex(0,1);     % 虚数 i を設定x
z0 = 2+2*i;           % 初期値
p = [1 0 0 -1];       % 多項式 p(x) の係数を設定
dp = polyder(p);      % p(x) を微分した多項式の格納
tol = 1.0e-5;         % 停止条件
n = 100;              % 最大反復回数
tz = roots(p);        % 真の値
z(1) = z0;            % 各反復における近似解を格納する配列

%-----------------------------------------------------------
% Newton Method
for k=1:n
    fz  = polyval(p,z(k));
    dfz = polyval(dp,z(k));

    z(k+1) = z(k) - fz / dfz;

    pz(k) = abs(polyval(p,z(k)));
    if pz(k) < tol
        break;
    end
end
%-----------------------------------------------------------

dis = tz - z(k);
find(dis == min(dis))