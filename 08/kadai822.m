% 色塗り
clear all;
m = 300ec;               % 分割数
xx = linspace(-4,4,m);
yy = linspace(-5,5,m);
C = zeros(m, m);

i = complex(0,1);      % 虚数
p = [1 0 0 -1];
dp = polyder(p);
tol = 1e-5;
n = 100;
tz = roots(p);         % 真の値

for s = 1:m
    for t = 1:m
        % -----------------------------------
        % 初期値z0の設定とNewton法による求解
        % -----------------------------------
        z0 = xx(s) + yy(t) * i;
        z(1) = z0;
        for k=1:n
            fz  = polyval(p,z(k));
            dfz = polyval(dp,z(k));
            
            z(k+1) = z(k) - fz / dfz;
            
            if abs(polyval(p,z(k+1))) < tol
                break;
            end
        end
        % -----------------------------------------------------
        % 変数indexに近似解と最も近い真の解のインデックスを格納
        % -----------------------------------------------------
        dis = abs(tz - z(k));
        C(t,s) = find(dis == min(dis));
        
    end
end
h = pcolor(xx,yy,C);
shading flat;

% saveas(h, '~/public_html/secure_htdocs/matlab/08/kadai822', 'pdf');