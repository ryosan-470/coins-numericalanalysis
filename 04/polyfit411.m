% 演習4 (1-1) 補間多項式の係数を求める
n = 5;
x = linspace(-0.3,4.0,n);         % n点の等間隔な点
f = sin(x).*exp(-x);              % 関数
polyfit(x,f,n-1)