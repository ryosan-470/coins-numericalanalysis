% æ¼ç¿? (1-2) ã°ã©ããæç»
n = 5;
x = linspace(-0.3,4.0,n);               % ÔuÉnÂÌ_ð¶¬
f = sin(x).*exp(-x);                    % Öl
p = polyfit(x,f,n-1);

xf = linspace(-0.3,4.0,500);             % fÌÍÍ
fx = sin(xf).*exp(-xf); 
hold on;
h = plot(xf,fx, '--b');                 % f(x)(ÂÌ_üOt)
h = plot(x,f,'o', 'MarkerSize', 15);    % âÔ_
h = plot(xf,polyval(p,xf), '-r');       % ß½½®Ot(ÔÌÀüOt)
hold off;
axis equal;
saveas(h,'../matlab_path/04/412_graph_axis_equal', 'pdf');