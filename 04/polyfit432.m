% 演習4　課題3 (3-1)
n = 10;
x = cos(-pi/(n-1)*(0:n-1));
% x = linspace(-1.0,1.0,n);               % 等間隔にn個の点を生成
f = 1./(25.*x.^2+5.*x+2);
p = polyfit(x,f,n-1)

xf = linspace(-1.0,1.0,500);             % fの範囲
fx = 1./(25.*xf.^2+5.*xf+2);
hold on;
h = plot(xf,fx, '--b');                 % f(x)(青の点線グラフ)
h = plot(x,f,'o', 'MarkerSize', 10);    % 補間点
h = plot(xf,polyval(p,xf), '-r');       % 求めた多項式グラフ(赤の実線グラフ)
hold off;
% axis equal;
saveas(h,'../matlab_path/04/432_graph_axis_equal', 'pdf');