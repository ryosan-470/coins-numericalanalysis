% ヴァンデル・モンド行列　
% x: 補間点
function [c] = Vandermonde(x)
    n = length(x);     % n は xの大きさ
    A = zeros(n,n);    % 大きさnのベクトルを初期化
    for k=1:n
        A(:,k) = (x').^(k-1);
    end
    c = fliplr(A);     % 行列の左右を反転する
end
