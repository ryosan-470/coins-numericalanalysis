# 数値計算法 (Introduction of Numerical Analysis)
筑波大学情報学群情報科学類2014年度開設授業科目 数値計算法

Introduction of Numerical Analysis, at 2014, College of Information Science, University of Tsukuba

## 中身 ##
ソースコード(.m)

## ライセンス ##
This repository is licensed under the public domain.
