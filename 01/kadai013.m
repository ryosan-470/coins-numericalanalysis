clear all;
rng(0, 'twister');

for idx=1:5
    tic
    n(idx) = 10 ^ idx;
    for i=1:n(idx)
        x(i) = rand();
        y(i) = rand();
    end
    for i=1:n(idx)
        r(i) = x(i)^2 + y(i)^2;
    end
    m = 0;
    for i=1:n(idx)
        if r(i) <= 1
            m = m + 1;
        end
    end
    semilogx(n(idx), toc, 'o')
    hold on;
end
