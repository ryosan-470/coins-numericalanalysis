% 課題2(2-2) 計算時間を返す
function [time, p] = MonteCarlo222(n)
    tic;
    % 変数に予め0を代入
    x = zeros(1,n);
    y = zeros(1,n);
    r = zeros(1,n);
    rng(0, 'twister');
    
    i=1:n;
    x = rand(1,n);
    y = rand(1,n);
    r = x.^2 + y.^2;
    
    m = 0;
    t = r <= 1;
    m = sum(t);

    p = 4*m/n;
    time = toc;
