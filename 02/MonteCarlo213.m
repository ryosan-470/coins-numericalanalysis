% 課題1(1-3) 計算時間を返す
function [time, p]= MonteCarlo213(n)
    tic;
    % 変数に予め0を代入
    x = zeros(1,n);
    y = zeros(1,n);
    r = zeros(1,n);
    rng(0, 'twister');
    for i=1:n
        x(i) = rand();
        y(i) = rand();
    end
    for i=1:n
        r(i) = x(i)^2 + y(i)^2;
    end
    m = 0;
    for i=1:n
        if r(i) <= 1
            m = m + 1;
        end
    end
    p = 4*m/n;
    time = toc;
