% 課題2(2-1) 計算時間を返す
function [time, p] = MonteCarlo221(n)
    tic;
    % 変数に予め0を代入
    x = zeros(1,n);
    y = zeros(1,n);
    r = zeros(1,n);
    rng(0, 'twister');
    i=1:n;
    x = rand(1, n);
    y = rand(1, n);
    
    for i=1:n
        r(i) = x(i)^2 + y(i)^2;
    end
    m = 0;
    for i=1:n
        if r(i) <= 1
            m = m + 1;
        end
    end
    p = 4*m/n;
    time = toc;
